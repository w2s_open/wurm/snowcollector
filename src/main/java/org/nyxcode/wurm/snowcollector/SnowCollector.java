package org.nyxcode.wurm.snowcollector;

import com.wurmonline.mesh.Tiles;
import com.wurmonline.server.Server;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.interfaces.Configurable;
import org.gotti.wurmunlimited.modloader.interfaces.PreInitable;
import org.gotti.wurmunlimited.modloader.interfaces.WurmServerMod;
import java.util.Properties;

/**
 * Created by whisp on 01.05.2017.
 */
public class SnowCollector implements WurmServerMod, Configurable, PreInitable {
    public void configure(Properties properties) {

    }


    public void preInit() {
        HookManager.getInstance().registerHook("com.wurmonline.server.behaviours.TileBehaviour", "canCollectSnow", null, () -> (object, method, args) -> {
            Integer tilex = (Integer) args[1];
            Integer tiley = (Integer) args[2];
            Byte type = (Byte) args[3];
            if(type == Tiles.Tile.TILE_SNOW.id) {
                Server.setGatherable(tilex, tiley, true);
                return true;
            }
            return method.invoke(object, args);
        });

    }
}
